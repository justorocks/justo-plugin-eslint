"use strict";

var _justo = require("justo");

var _child_process = require("child_process");

var _child_process2 = _interopRequireDefault(_child_process);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//Plugin task.
//imports
module.exports = exports = (0, _justo.simple)({
  id: "eslint",
  desc: "Lint JavaScript code.",
  fmt(params) {
    return "Lint code: " + (params.src instanceof Array ? params.src : [params.src]).join(", ");
  },
  fmtParams(params) {
    return { src: params.length == 0 ? ["."] : params };
  }
}, function eslint(params, done) {
  var cmd = "./node_modules/.bin/eslint";
  if (params.conf) cmd += " -c " + params.conf;
  cmd += " " + (params.src instanceof Array ? params.src : [params.src]).join(" ");
  _child_process2.default.exec(cmd, done);
});