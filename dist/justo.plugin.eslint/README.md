# justo.plugin.eslint

[![NPM version](https://img.shields.io/npm/v/justo.plugin.eslint.svg)](https://npmjs.org/package/justo.plugin.eslint)
[![Total downloads](https://img.shields.io/npm/dt/justo.plugin.eslint.svg)](https://npmjs.org/package/justo.plugin.eslint)

**Justo** plugin for **ESLint**.

*Proudly made with ♥ in Valencia, Spain, EU.*

## Use

```
const eslint = require("justo.plugin.eslint");
```

## eslint task

This task runs `./node_modules/.bin/eslint`:

```
eslint({conf, src});
```

- `conf` (string). `.eslintrc` to use. Default: `./.eslintrc`.
- `src` (string or string[], required). Source file(s) to parse.

Example:

```
eslint({
  src: ["Justo.cat.js", "src", "test"]
});
```

### fmtParams()

The `fmtParams()` has been defined for being used as follows:

```
eslint(src:string=".")
```
