//imports
const {catalog} = require("justo");
const babel = require("justo.plugin.babel");
const cli = require("justo.plugin.cli");
const fs = require("justo.plugin.fs");
const npm = require("justo.plugin.npm");

//internal data
const PKG = require("./package").name;

//catalog
catalog.macro("lint", [
  [cli, "./node_modules/.bin/eslint ."]
]).title("Lint source code");

catalog.macro("trans-js", [
  {task: babel, params: {src: "src", dst: `dist/${PKG}/src/`}}
]).title("Transpile from JS to JS");

catalog.macro("build", [
  catalog.get("trans-js"),
  [fs.copy, "package.json", `dist/${PKG}/package.json`],
  [fs.copy, "README.md", `dist/${PKG}/README.md`],
]).title("Build package");

catalog.macro("make", [
  [fs.rm, "./dist"],
  catalog.get("lint"),
  catalog.get("build")
]).title("Lint and build");

catalog.call("install", npm.install, {
  pkg: `dist/${PKG}`,
  global: true
}).title("Install package globally");

catalog.call("pub", npm.publish, {
  who: "justojs",
  path: `dist/${PKG}`
}).title("Publish in NPM");

catalog.macro("dflt", [
  catalog.get("make")
]).title("Lint and build");
